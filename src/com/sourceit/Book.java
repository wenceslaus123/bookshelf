package com.sourceit;

public class Book {
    String author;
    int year;

    public Book(String author, int year) {
        this.author = author;
        this.year = year;
    }

    public boolean isOlderThen(Book book) {
        return year < book.year;
    }
}
