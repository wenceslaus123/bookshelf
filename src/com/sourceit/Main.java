package com.sourceit;

import static com.sourceit.Generator.generateBooks;

public class Main {

    public static void main(String[] args) {
	    BookShelf bookShelf = new BookShelf(generateBooks());
        Book oldBook = bookShelf.getOldestBook();
        System.out.println(oldBook.author);
    }
}
