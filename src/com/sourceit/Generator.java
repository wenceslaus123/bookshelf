package com.sourceit;

/**
 * Created by wenceslaus on 28.11.17.
 */
public final class Generator {

    private Generator() {
    }

    public static Book[] generateBooks() {
        Book[] books = new Book[3];
        books[0] = new Book("Tokien", 1920);
        books[0] = new Book("Tolstoy", 1820);
        books[0] = new Book("Pushkin", 1720);
        return books;
    }

}
