package com.sourceit;

/**
 * Created by wenceslaus on 28.11.17.
 */
public class BookShelf {

    Book[] books;

    public BookShelf(Book[] books) {
        this.books = books;
    }

    public Book getOldestBook() {
        Book book = books[0];
        for (int i = 1; i < books.length; i++) {
            if (books[i].isOlderThen(book)) {
                book = books[i];
            }
        }
        return book;
    }
}
